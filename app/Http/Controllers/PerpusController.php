<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Buku;

class PerpusController extends Controller
{
    public function index(){
    	$databuku = Buku::all();
    	return view('perpus/index' , ['databuku' => $databuku]);
    }

    public function show($id){

    	$buku = Buku::find($id);

    	// if(!$buku){
     //        abort('404');
     //    }

    	return view('perpus/lihat/single' , ['buku' => $buku]);
    }

    public function create_buku(){

    	$penerbit   = DB::table('penerbit')->get();
    	$pengarang  = DB::table('pengarang')->get();
    	$isbn       = DB::table('isbn')->get();
    	$thn_terbit = DB::table('thn_terbit')->get();
    	$kategori   = DB::table('kategori')->get();
    	$kondisi    = DB::table('kondisi')->get();

    	return view('perpus/createbuku' , ['penerbit' => $penerbit , 'pengarang' => $pengarang , 'isbn' => $isbn , 'thn_terbit' => $thn_terbit , 'kategori' => $kategori , 'kondisi' => $kondisi]);
    }

    public function store_buku(Request $req){
    	$buku = new Buku;
    	$buku->judul = $req->judul;
    	$buku->penerbit = $req->penerbit;
    	$buku->pengarang = $req->pengarang;
    	$buku->isbn = $req->isbn;
    	$buku->stok = $req->stok;
    	$buku->thn_terbit = $req->thn_terbit;
    	$buku->kategori = $req->kategori;
    	$buku->kondisi = $req->kondisi;

    	$buku->save();

    	return redirect('perpus');
    }

    public function edit($id){
    	$buku = Buku::find($id);

    	// if(!$buku){
     //        abort('404');
     //    }

        $penerbit   = DB::table('penerbit')->get();
    	$pengarang  = DB::table('pengarang')->get();
    	$isbn       = DB::table('isbn')->get();
    	$thn_terbit = DB::table('thn_terbit')->get();
    	$kategori   = DB::table('kategori')->get();
    	$kondisi    = DB::table('kondisi')->get();


    	return view('perpus/edit' , ['buku' => $buku , 'penerbit' => $penerbit , 'pengarang' => $pengarang , 'isbn' => $isbn , 'thn_terbit' => $thn_terbit , 'kategori' => $kategori , 'kondisi' => $kondisi]);
    }

    public function update(Request $req , $id){

        $buku = Buku::find($id);
        $buku->judul = $req->judul;
        $buku->pengarang = $req->pengarang;
        $buku->penerbit = $req->penerbit;
        $buku->isbn = $req->isbn;
        $buku->stok = $req->stok;
        $buku->thn_terbit = $req->thn_terbit;
        $buku->kategori = $req->kategori;
        $buku->kondisi = $req->kondisi;
        $buku->save();


        return redirect('perpus/' . $id);
    }

    public function destroy($id){
        $buku = Buku::find($id);
        $buku->delete();

        return redirect('perpus');
    }

    public function create_member(){

    	return view('perpus/createmember');
    }

    public function store_member(Request $req){

    	$this->validate($req , [
            'username' => 'required|min:6',
            'password'  => 'required|min:8'
        ]);

    	DB::table('anggota')->insert([
    		['nama' => $req->nama , 
    		 'username' => $req->username ,
    		 'tgl_lahir' => $req->tgl_lahir  ,
    		 'jk' => $req->jk ,
    		 'alamat' => $req->alamat ,
    		 'no_telp' => $req->no_telp ]
    	]);

    	DB::table('login')->insert([
    		['jenis_user' => 'Anggota' , 
    		 'username' => $req->username ,
    		 'password' => $req->password ]
    	]);

    	return redirect('perpus/dataanggota');
    }

    public function data_anggota(){
    	$dataanggota = DB::table('anggota')->get();

    	return view('perpus/dataanggota' , [ 'dataanggota' => $dataanggota ]);
    }

    public function peminjaman(){

    	$buku = DB::table('buku')->get();

    	return view('perpus/peminjaman' , [ 'buku' => $buku ]);
    }

    public function store_peminjaman(Request $req){

    	// $buku = DB::table('buku')->where('judul' , $req->buku)->get('stok');
    	$buku = DB::table('buku')->select('stok')->where('judul' , $req->buku)->first();
    	
    	DB::table('peminjaman')->insert([
    		[ 'username' => $req->username,
    		  'judul' => $req->buku,
    		  'tgl_pinjam' => $req->tgl_pinjam,
    		  'tgl_kembali' => $req->tgl_kembali,
    		  'jml_buku' => $req->jml_buku]
    	]);

    	// DB::table('buku')->where('judul' , $req->buku)->update([ 'stok' => $buku ]);

    	return redirect('perpus/datapeminjaman');
    }

    public function data_peminjaman(){
    	$datapeminjaman = DB::table('peminjaman')->get();

    	return view('perpus/datapeminjaman' , ['datapeminjaman' => $datapeminjaman]);
    }
}
