<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/perpus', 'PerpusController@index');

Route::get('/perpus/create_buku', 'PerpusController@create_buku');
Route::post('/perpus/create_buku/create_buku_proses', 'PerpusController@store_buku');

Route::get('/perpus/lihat/{id}', 'PerpusController@show');

Route::get('/perpus/{id}/edit', 'PerpusController@edit');
Route::put('/perpus/{id}', 'PerpusController@update');

Route::delete('/perpus/{id}', 'PerpusController@destroy');

Route::get('/perpus/create_member', 'PerpusController@create_member');
Route::post('/perpus/create_member/create_member_proses', 'PerpusController@store_member');

Route::get('/perpus/data_anggota', 'PerpusController@data_anggota');

Route::get('/perpus/peminjaman', 'PerpusController@peminjaman');
Route::post('/perpus/peminjaman/peminjaman_proses', 'PerpusController@store_peminjaman');

Route::get('/perpus/data_peminjaman', 'PerpusController@data_peminjaman');