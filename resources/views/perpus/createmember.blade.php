@extends('layout.master')

@section('title' , ' Halaman Create Anggota')


@section('content')

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Anggota Baru</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<form action="//localhost/project_perpus/public/perpus/create_member/create_member_proses" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Lahir : </label>
										<input class="form-control" type="date" name="tgl_lahir" value="{{ old('tgl_lahir') }}">
									</div>
									<div class="form-group">
										<label>Jenis Kelamin : </label>
										<select name="jk" class="form-control">
											<option value="Laki-laki">Laki-laki</option>
											<option value="Perempuan">Perempuan</option>
										</select>
									</div>
									<div class="form-group">
										<label>No Telepon : </label>
										<input class="form-control" type="text" name="no_telp" value="{{ old('no_telp') }}">
									</div>
									<div class="form-group">
										<label>Alamat : </label>
										<textarea  class="form-control" name="alamat">{{ old('alamat') }}</textarea>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama Lengkap : </label>
										<input class="form-control" type="text" name="nama" value="{{ old('nama') }}">
									</div>
									<div class="form-group">
										<label>Username : </label>
										<input class="form-control" type="text" name="username" value="{{ old('username') }}">
									</div>
									<div class="form-group">
										<label>Password : </label>
										<input class="form-control" type="password" name="password" value="{{ old('password') }}">
									</div>
								</div>

								<div class="col-lg-12">
									<input type="submit" name="submit" value="Create" class="btn btn-success">
									{{ csrf_field() }}
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
@endsection
