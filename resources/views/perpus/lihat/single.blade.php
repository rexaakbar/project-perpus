@extends('layout.master')

@section('title' , ' Halaman Show')

@section('content')

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Data Buku</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Judul Buku : </label>
									<label>{{ $buku->judul }}</label>
								</div>
								<div class="form-group">
									<label>Penerbit : </label>
									<label>{{ $buku->penerbit }}</label>
								</div>
								<div class="form-group">
									<label>Pengarang : </label>
									<label>{{ $buku->pengarang }}</label>
								</div>
								<div class="form-group">
									<label>ISBN : </label>
									<label>{{ $buku->isbn }}</label>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<label>Stok : </label>
									<label>{{ $buku->stok }}</label>
								</div>
								<div class="form-group">
									<label>Tahun Terbit : </label>
									<label>{{ $buku->thn_terbit }}</label>
								</div>
								<div class="form-group">
									<label>Kategori : </label>
									<label>{{ $buku->kategori }}</label>
								</div>
								<div class="form-group">
									<label>Kondisi : </label>
									<label>{{ $buku->kondisi }}</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
@endsection
