@extends('layout.master')

@section('title' , ' Halaman Form Peminjaman')

@php
	$tanggal = date('Y-m-d');
@endphp


@section('content')

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Form Peminjaman</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<form action="//localhost/project_perpus/public/perpus/peminjaman/peminjaman_proses" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Username : </label>
										<input class="form-control" type="text" name="username" value="{{ old('username') }}">
									</div>
									<div class="form-group">
										<label>Buku Yang Di Pinjam : </label>
										<select name="buku" class="form-control">
											@foreach($buku as $bukuu)
												<option value="{{ $bukuu->judul }}">{{ $bukuu->judul }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal Pinjam : </label>
										<input class="form-control" type="date" name="tgl_pinjam" value="@php echo $tanggal; @endphp">
									</div>
									<div class="form-group">
										<label>Tanggal Kembali : </label>
										<input class="form-control" type="date" name="tgl_kembali" min="@php echo $tanggal; @endphp">
									</div>
									<div class="form-group">
										<label>Jumlah buku : </label>
										<input class="form-control" type="number" name="jml_buku" min="1">
									</div>
								</div>

								<div class="col-lg-12">
									<input type="submit" name="submit" value="Create" class="btn btn-success">
									{{ csrf_field() }}
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
@endsection
