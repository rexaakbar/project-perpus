@extends('layout.master')

@section('title' , 'Halaman Edit')



@section('content')

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit Data Buku</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<form action="//localhost/project_perpus/public/perpus/{{ $buku->id }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Judul Buku</label>
										<input class="form-control" type="text" name="judul" value="{{ $buku->judul }}">
									</div>
									<div class="form-group">
										<label>Penerbit</label>
										<select name="penerbit" class="form-control">
											@foreach($penerbit as $dpen)
												@if($dpen->penerbit == $buku->penerbit)
													<option value="{{ $dpen->penerbit }}" selected>{{ $dpen->penerbit }}</option>
												@else
													<option value="{{ $dpen->penerbit }}">{{ $dpen->penerbit }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Pengarang</label>
										<select name="pengarang" class="form-control">
											@foreach($pengarang as $dpeng)
												@if($dpeng->pengarang == $buku->pengarang)
													<option value="{{ $dpeng->pengarang }}" selected>{{ $dpeng->pengarang }}</option>
												@else
													<option value="{{ $dpeng->pengarang }}">{{ $dpeng->pengarang }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>ISBN</label>
										<select name="isbn" class="form-control">
											@foreach($isbn as $dis)
												@if($dis->isbn == $buku->isbn)
													<option value="{{ $dis->isbn }}" selected>{{ $dis->isbn }}</option>
												@else
													<option value="{{ $dis->isbn }}">{{ $dis->isbn }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<label>Stok</label>
										<input class="form-control" type="number" name="stok" min="1" value="{{ $buku->stok }}">
									</div>
									<div class="form-group">
										<label>Tahun Terbit</label>
										<select name="thn_terbit" class="form-control">
											@foreach($thn_terbit as $dthn)
												@if($dthn->thn_terbit == $buku->thn_terbit)
													<option value="{{ $dthn->thn_terbit }}" selected>{{ $dthn->thn_terbit }}</option>
												@else
													<option value="{{ $dthn->thn_terbit }}">{{ $dthn->thn_terbit }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Kategori</label>
										<select name="kategori" class="form-control">
											@foreach($kategori as $dkat)
												@if($dkat->kategori == $buku->kategori)
													<option value="{{ $dkat->kategori }}" selected>{{ $dkat->kategori }}</option>
												@else
													<option value="{{ $dkat->kategori }}">{{ $dkat->kategori }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Kondisi</label>
										<select name="kondisi" class="form-control">
											@foreach($kondisi as $dkon)
												@if($dkon->kondisi == $buku->kondisi)
													<option value="{{ $dkon->kondisi }}" selected>{{ $dkon->kondisi }}</option>
												@else
													<option value="{{ $dkon->kondisi }}">{{ $dkon->kondisi }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-lg-12">
									<input type="submit" name="submit" value="Edit" class="btn btn-success">
									{{ csrf_field() }}
									<input type="hidden" name="_method" value="PUT">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
@endsection
