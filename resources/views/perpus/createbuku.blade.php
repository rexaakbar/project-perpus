@extends('layout.master')

@section('title' , ' Halaman Create')


@section('content')

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Data Buku</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<form action="//localhost/project_perpus/public/perpus/create_buku/create_buku_proses" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Judul Buku</label>
										<input class="form-control" type="text" name="judul" value="{{ old('judul') }}">
									</div>
									<div class="form-group">
										<label>Penerbit</label>
										<select name="penerbit" class="form-control">
											@foreach($penerbit as $dpen)
												<option value="{{ $dpen->penerbit }}">{{ $dpen->penerbit }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Pengarang</label>
										<select name="pengarang" class="form-control">
											@foreach($pengarang as $dpeng)
												<option value="{{ $dpeng->pengarang }}">{{ $dpeng->pengarang }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>ISBN</label>
										<select name="isbn" class="form-control">
											@foreach($isbn as $dis)
												<option value="{{ $dis->isbn }}">{{ $dis->isbn }}</option>
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<label>Stok</label>
										<input class="form-control" type="number" name="stok" min="1" value="{{ old('stok') }}">
									</div>
									<div class="form-group">
										<label>Tahun Terbit</label>
										<select name="thn_terbit" class="form-control">
											@foreach($thn_terbit as $dthn)
												<option value="{{ $dthn->thn_terbit }}">{{ $dthn->thn_terbit }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Kategori</label>
										<select name="kategori" class="form-control">
											@foreach($kategori as $dkat)
												<option value="{{ $dkat->kategori }}">{{ $dkat->kategori }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Kondisi</label>
										<select name="kondisi" class="form-control">
											@foreach($kondisi as $dkon)
												<option value="{{ $dkon->kondisi }}">{{ $dkon->kondisi }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-lg-12">
									<input type="submit" name="submit" value="Create" class="btn btn-success">
									{{ csrf_field() }}
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
@endsection
