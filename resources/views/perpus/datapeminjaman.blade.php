@extends('layout.master')

@section('title' , 'Halaman Data Peminjaman')

@section('content')
@php
	$htg = 0;	
@endphp
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Username</th>
										<th>Judul Buku</th>
										<th>Tanggal Pinjam</th>
										<th>Tanggal Di Kembalikan</th>
										<th>Di Kembalikan</th>
										<th>Denda</th>
										<th>Jumlah Buku</th>
										{{-- <th></th> --}}

									</tr>
								</thead>
								<tbody>
									@foreach($datapeminjaman as $pinjam)
										<tr class="odd gradeA">
											<td>@php $htg++; echo $htg; @endphp</td>
											<td>{{ $pinjam->username }}</td>
											<td>{{ $pinjam->judul }}</td>
											<td>{{ $pinjam->tgl_pinjam }}</td>
											<td>{{ $pinjam->tgl_kembali }}</td>
											<td>{{ $pinjam->kembalikan }}</td>
											<td>{{ $pinjam->denda }}</td>
											<td>{{ $pinjam->jml_buku }}</td>
											{{-- <td>
											<form action="//localhost/project_perpus/public/perpus/{{ $buku->id }}" method="post" accept-charset="utf-8">
			
												<input type="submit" name="submit" value="delete" class="btn btn-danger">

												{{ csrf_field() }}

												<input type="hidden" name="_method" value="DELETE">
											</form>

											<a href="//localhost/project_perpus/public/perpus/{{ $buku->id }}/edit" title=""><div class="btn btn-info"><i class="fa fa-info fa-fw"></i></div></a>

											</td> --}}
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					<div class="panel-footer"></div>
				</div>
			</div>
		</div>

	

	
	
	
@endsection

	{{-- @if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error )
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif --}}

	{{-- <h1>Ini halaman create menggunakan H1</h1>
	
	<form action="//localhost/laravel/public/blog" method="post" accept-charset="utf-8">
		
		<input type="text" name="title" value="{{  old('title') }}"> <br>
		
		
			@if($errors->has('title'))
				<p> {{  $errors->first('title') }}</p>
			@endif
		

		<textarea name="desc" >{{  old('desc') }}</textarea>  <br>

		@if($errors->has('desc'))
				<p> {{  $errors->first('desc') }}</p>
			@endif
		
		<input type="submit" name="submit" value="Create">

		{{ csrf_field() }}
		
	</form> --}}