<!DOCTYPE html>
<html>

<?php 
    date_default_timezone_set("Asia/Jakarta");
 ?>

<head>
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('font-awesome/css/font-awesome.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('metisMenu/metisMenu.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/sb-admin-2.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('morrisjs/morris.css')); ?>">
	
	
</head>
<body>

	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="//localhost/project_perpus/public/perpus">Halaman Admin</a>
        </div>

        <div class="navbar-default sidebar" role="navigation" >
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="//localhost/project_perpus/public/perpus/create_buku" style="color:rgba(0,0,0,1);"><i class="glyphicon glyphicon-plus fa-fw"></i><b>Tambah buku</b></a>
                        </li>
                        <li>
                            <a href="//localhost/project_perpus/public/perpus/create_member" style="color:rgba(0,0,0,1);"><i class="glyphicon glyphicon-plus fa-fw"></i><b>Tambah anggota</b></a>
                        </li>
                        <li>
                            <a href="//localhost/project_perpus/public/perpus/data_anggota" style="color:rgba(0,0,0,1);"><i class="fa fa-key fa-fw"></i><b>Data Anggota</b></a>
                        </li>
                        <li>
                            <a href="//localhost/project_perpus/public/perpus/peminjaman" style="color:rgba(0,0,0,1);"><i class="fa fa-bar-chart-o fa-fw"></i><b>Form Peminjaman</b></a>
                        </li>
                        <li>
                            <a href="//localhost/project_perpus/public/perpus/data_peminjaman" style="color:rgba(0,0,0,1);"><i class="fa fa-key fa-fw"></i><b>Data Peminjaman</b></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>

    </nav>

	<div id="page-wrapper">
		<?php echo $__env->yieldContent('content'); ?>
	</div>


	</div>
    

	




	
	<script src="<?php echo e(URL::asset('jquery/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('datatables/js/jquery.dataTables.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('datatables-plugins/dataTables.bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('datatables-responsive/dataTables.responsive.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('metisMenu/metisMenu.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('raphael/raphael.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('morrisjs/morris.min.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('data/morris-data.js')); ?>"></script>
	<script src="<?php echo e(URL::asset('dist/js/sb-admin-2.js')); ?>"></script>
	
	<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

	
</body>
</html>

<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Load bootstrap</title>
    

</head>
<body>
    <h1>Hello, world!</h1>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


</body>
</html> -->