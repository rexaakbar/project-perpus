<?php $__env->startSection('title' , 'Halaman Edit'); ?>



<?php $__env->startSection('content'); ?>

	
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit Data Buku</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<form action="//localhost/project_perpus/public/perpus/<?php echo e($buku->id); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Judul Buku</label>
										<input class="form-control" type="text" name="judul" value="<?php echo e($buku->judul); ?>">
									</div>
									<div class="form-group">
										<label>Penerbit</label>
										<select name="penerbit" class="form-control">
											<?php $__currentLoopData = $penerbit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dpen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dpen->penerbit == $buku->penerbit): ?>
													<option value="<?php echo e($dpen->penerbit); ?>" selected><?php echo e($dpen->penerbit); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dpen->penerbit); ?>"><?php echo e($dpen->penerbit); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
									<div class="form-group">
										<label>Pengarang</label>
										<select name="pengarang" class="form-control">
											<?php $__currentLoopData = $pengarang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dpeng): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dpeng->pengarang == $buku->pengarang): ?>
													<option value="<?php echo e($dpeng->pengarang); ?>" selected><?php echo e($dpeng->pengarang); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dpeng->pengarang); ?>"><?php echo e($dpeng->pengarang); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
									<div class="form-group">
										<label>ISBN</label>
										<select name="isbn" class="form-control">
											<?php $__currentLoopData = $isbn; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dis->isbn == $buku->isbn): ?>
													<option value="<?php echo e($dis->isbn); ?>" selected><?php echo e($dis->isbn); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dis->isbn); ?>"><?php echo e($dis->isbn); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<label>Stok</label>
										<input class="form-control" type="number" name="stok" min="1" value="<?php echo e($buku->stok); ?>">
									</div>
									<div class="form-group">
										<label>Tahun Terbit</label>
										<select name="thn_terbit" class="form-control">
											<?php $__currentLoopData = $thn_terbit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dthn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dthn->thn_terbit == $buku->thn_terbit): ?>
													<option value="<?php echo e($dthn->thn_terbit); ?>" selected><?php echo e($dthn->thn_terbit); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dthn->thn_terbit); ?>"><?php echo e($dthn->thn_terbit); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
									<div class="form-group">
										<label>Kategori</label>
										<select name="kategori" class="form-control">
											<?php $__currentLoopData = $kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dkat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dkat->kategori == $buku->kategori): ?>
													<option value="<?php echo e($dkat->kategori); ?>" selected><?php echo e($dkat->kategori); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dkat->kategori); ?>"><?php echo e($dkat->kategori); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
									<div class="form-group">
										<label>Kondisi</label>
										<select name="kondisi" class="form-control">
											<?php $__currentLoopData = $kondisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dkon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($dkon->kondisi == $buku->kondisi): ?>
													<option value="<?php echo e($dkon->kondisi); ?>" selected><?php echo e($dkon->kondisi); ?></option>
												<?php else: ?>
													<option value="<?php echo e($dkon->kondisi); ?>"><?php echo e($dkon->kondisi); ?></option>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>

								<div class="col-lg-12">
									<input type="submit" name="submit" value="Edit" class="btn btn-success">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="_method" value="PUT">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>