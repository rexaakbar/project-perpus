<?php $__env->startSection('title' , ' Halaman Show'); ?>

<?php $__env->startSection('content'); ?>

	
	
	<div id="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Data Buku</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Harap Semua Data Di Isi
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Judul Buku : </label>
									<label><?php echo e($buku->judul); ?></label>
								</div>
								<div class="form-group">
									<label>Penerbit : </label>
									<label><?php echo e($buku->penerbit); ?></label>
								</div>
								<div class="form-group">
									<label>Pengarang : </label>
									<label><?php echo e($buku->pengarang); ?></label>
								</div>
								<div class="form-group">
									<label>ISBN : </label>
									<label><?php echo e($buku->isbn); ?></label>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<label>Stok : </label>
									<label><?php echo e($buku->stok); ?></label>
								</div>
								<div class="form-group">
									<label>Tahun Terbit : </label>
									<label><?php echo e($buku->thn_terbit); ?></label>
								</div>
								<div class="form-group">
									<label>Kategori : </label>
									<label><?php echo e($buku->kategori); ?></label>
								</div>
								<div class="form-group">
									<label>Kondisi : </label>
									<label><?php echo e($buku->kondisi); ?></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>