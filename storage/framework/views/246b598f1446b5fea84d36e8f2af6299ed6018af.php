<?php $__env->startSection('title' , 'Halaman Utama'); ?>

<?php $__env->startSection('content'); ?>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
						<div class="panel-body">
							<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Judul</th>
										<th>Penerbit</th>
										<th>Pengarang</th>
										<th>ISBN</th>
										<th>Stok</th>
										<th>Kategori</th>
										<th>Kondisi</th>
										<th></th>

									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $databuku; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $buku): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr class="odd gradeA">
											<td><?php echo e($buku->id); ?></td>
											<td><?php echo e($buku->judul); ?></td>
											<td><?php echo e($buku->penerbit); ?></td>
											<td><?php echo e($buku->pengarang); ?></td>
											<td><?php echo e($buku->isbn); ?></td>
											<td><?php echo e($buku->stok); ?></td>
											<td><?php echo e($buku->kategori); ?></td>
											<td><?php echo e($buku->kondisi); ?></td>
											<td>
											<form action="//localhost/project_perpus/public/perpus/<?php echo e($buku->id); ?>" method="post" accept-charset="utf-8">
			
												<input type="submit" name="submit" value="delete" class="btn btn-danger">

												<?php echo e(csrf_field()); ?>


												<input type="hidden" name="_method" value="DELETE">
											</form>

											<a href="//localhost/project_perpus/public/perpus/<?php echo e($buku->id); ?>/edit" title=""><div class="btn btn-info"><i class="fa fa-info fa-fw"></i></div></a>

											</td>
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					<div class="panel-footer"></div>
				</div>
			</div>
		</div>

	

	
	
	
<?php $__env->stopSection(); ?>

	

	
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>