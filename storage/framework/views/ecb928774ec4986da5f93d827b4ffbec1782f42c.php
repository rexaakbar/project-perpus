<!DOCTYPE html>
<html>
<head>
	<title><?php echo $__env->yieldContent('title'); ?></title>
</head>
<body>
	<header>
		<nav>
			<a href="../public" title="">Home</a>
			<a href="../blog" title="">Blog</a>
		</nav>	
	</header><!-- /header -->
	<br>
	<?php echo $__env->yieldContent('content'); ?>
	<br>
	<footer>
		<p>
			&copy; laravel & belajar laravel
		</p>
	</footer>
</body>
</html>

<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Load bootstrap</title>
    
    <?php echo e(HTML::style('bootstrap/css/bootstrap.min.css')); ?>

</head>
<body>
    <h1>Hello, world!</h1>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<?php echo e(HTML::script('bootstrap/js/bootstrap.min.js')); ?>

</body>
</html> -->