-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2017 at 08:15 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbperpuslaravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE IF NOT EXISTS `anggota` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(255) NOT NULL,
  `alamat` text,
  `no_telp` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `username`, `nama`, `tgl_lahir`, `jk`, `alamat`, `no_telp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'rexaakbar', 'Rexa Akbar Malik', '2000-01-03', 'Laki-laki', 'Jamika', '083821857496', NULL, '2017-07-18 05:19:58', NULL),
(2, 'rexaakbar', 'Rexa Akbar Malik', '2000-01-03', 'Laki-laki', 'Jamika', '083821857496', NULL, '2017-07-18 05:20:27', NULL),
(3, 'rexaakbar', 'Rexa Akbar Malik', '2000-01-03', 'Laki-laki', 'Jamika', '083821857496', NULL, '2017-07-18 05:20:53', NULL),
(4, 'asdasdasdasd', 'asdasdasd', '2017-07-07', 'Laki-laki', '123', '123', NULL, '2017-07-21 02:59:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE IF NOT EXISTS `buku` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penerbit` varchar(255) NOT NULL,
  `pengarang` varchar(255) NOT NULL,
  `isbn` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `thn_terbit` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `kondisi` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `judul`, `penerbit`, `pengarang`, `isbn`, `stok`, `thn_terbit`, `kategori`, `kondisi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Rumah Sakit', 'tzMBYo3MFZECksp', 'VPEEPe2zLNUzoYA', 'x3N8EuQV1eeFrDA', 80, 'YbKRrwid7XdbD2d', 'yGSD3wzlN1hGxTi', 'DyWRmnoPTtScFZQ', NULL, '2017-07-20 07:03:41', NULL),
(2, 'Rumah Sehat', 'Ga tau', 'Apalagi ini', 'ga tau sumpah', 90, '2000', 'Majalah', 'Kurang Baik', NULL, NULL, NULL),
(3, 'Jeritan si bisu', 'h1RNq32nmMdeFrg', 'IBZyOidZWRHEBRa', 'QIvyxGV5d3nYkEw', 20, 'YbKRrwid7XdbD2d', 'yGSD3wzlN1hGxTi', '8kiddW6pkogymrS', '2017-07-13 23:40:38', '2017-07-13 23:40:38', NULL),
(4, 'Nyoba aja', 't9lhtVcl3qkI1R9', 'zroLqkavlAqbFiY', 'gdHMwyC1qVkeuMP', 80, 'b3MoBPO219VQ2ws', 'rLNsL8bsqmaK7HI', 'kAKcJ2X9A9wBJGp', '2017-07-20 19:56:35', '2017-07-21 03:12:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `isbn`
--

CREATE TABLE IF NOT EXISTS `isbn` (
  `id_isbn` int(11) NOT NULL,
  `isbn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `isbn`
--

INSERT INTO `isbn` (`id_isbn`, `isbn`) VALUES
(1, 'gdHMwyC1qVkeuMP'),
(2, 'OdLhNEiuuKgAbqw'),
(3, 'gi2PRQGYy6nt5hf'),
(4, 'QIvyxGV5d3nYkEw'),
(5, 'x3N8EuQV1eeFrDA');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'rLNsL8bsqmaK7HI'),
(2, 'Y6ZzvJclymtNie5'),
(3, '7yyEkemYR0WuNQu'),
(4, 'NNCFkgse7LwNSKY'),
(5, 'yGSD3wzlN1hGxTi');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi`
--

CREATE TABLE IF NOT EXISTS `kondisi` (
  `id_kondisi` int(11) NOT NULL,
  `kondisi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kondisi`
--

INSERT INTO `kondisi` (`id_kondisi`, `kondisi`) VALUES
(1, 'kAKcJ2X9A9wBJGp'),
(2, 'DyWRmnoPTtScFZQ'),
(3, '8kiddW6pkogymrS'),
(4, 'oPeO27Ud9gsBmlr'),
(5, 'CfHspkAtuWMjKJo');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL,
  `jenis_user` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `jenis_user`, `username`, `password`) VALUES
(1, 'Anggota', 'rexaakbar', '1234567'),
(2, 'Anggota', 'rexaakbar', '123456'),
(3, 'Anggota', 'asdasdasdasd', 'asdasdasdasdasdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `kembalikan` date DEFAULT NULL,
  `denda` int(11) NOT NULL DEFAULT '0',
  `jml_buku` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `username`, `judul`, `tgl_pinjam`, `tgl_kembali`, `kembalikan`, `denda`, `jml_buku`) VALUES
(1, 'asdasdasd', 'Nyoba aja', '2017-07-21', '2017-07-26', NULL, 0, 20);

--
-- Triggers `peminjaman`
--
DELIMITER $$
CREATE TRIGGER `kurangstokbuku` AFTER INSERT ON `peminjaman`
 FOR EACH ROW BEGIN
	UPDATE buku SET stok = stok - NEW.jml_buku where judul = NEW.judul;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `penerbit`
--

CREATE TABLE IF NOT EXISTS `penerbit` (
  `id_penerbit` int(11) NOT NULL,
  `penerbit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerbit`
--

INSERT INTO `penerbit` (`id_penerbit`, `penerbit`) VALUES
(1, 't9lhtVcl3qkI1R9'),
(2, '933JCnHqPXgr0J5'),
(3, 'ycy1EFdESLkaR32'),
(4, 'Cmb89Tn3I2ICr6b'),
(5, 'EAw8tRVCxlQwb7H'),
(6, 'h1RNq32nmMdeFrg'),
(7, 'jz4fDgxN3qimj7T'),
(8, 'tzMBYo3MFZECksp'),
(9, 'h82oAYOyFFYvzp3'),
(10, 'Ola5UFTbrt24jAg');

-- --------------------------------------------------------

--
-- Table structure for table `pengarang`
--

CREATE TABLE IF NOT EXISTS `pengarang` (
  `id_pengarang` int(11) NOT NULL,
  `pengarang` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengarang`
--

INSERT INTO `pengarang` (`id_pengarang`, `pengarang`) VALUES
(1, 'zroLqkavlAqbFiY'),
(2, 'gY4LtkwkooX9T7j'),
(3, 'pCfk7SEDINB8bM2'),
(4, 'IBZyOidZWRHEBRa'),
(5, 'lAk8jr4raMxHeym'),
(6, 'VPEEPe2zLNUzoYA'),
(7, 'xEMOwg71MDmagId'),
(8, 'OdbFoUN0LZ6tgRD'),
(9, 'kcKoaCe1WVHYxv2'),
(10, 'B1zpuOjhkJ57qo9');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE IF NOT EXISTS `pengurus` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `thn_terbit`
--

CREATE TABLE IF NOT EXISTS `thn_terbit` (
  `id_thn_terbit` int(11) NOT NULL,
  `thn_terbit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thn_terbit`
--

INSERT INTO `thn_terbit` (`id_thn_terbit`, `thn_terbit`) VALUES
(1, 'b3MoBPO219VQ2ws'),
(2, 'muxV3DnruBjqaGO'),
(3, 'YbKRrwid7XdbD2d'),
(4, 'rRvvxRM9RYSrek9'),
(5, 'D9eRVZAl1DRggOV');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isbn`
--
ALTER TABLE `isbn`
  ADD PRIMARY KEY (`id_isbn`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kondisi`
--
ALTER TABLE `kondisi`
  ADD PRIMARY KEY (`id_kondisi`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`id_penerbit`);

--
-- Indexes for table `pengarang`
--
ALTER TABLE `pengarang`
  ADD PRIMARY KEY (`id_pengarang`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thn_terbit`
--
ALTER TABLE `thn_terbit`
  ADD PRIMARY KEY (`id_thn_terbit`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `isbn`
--
ALTER TABLE `isbn`
  MODIFY `id_isbn` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kondisi`
--
ALTER TABLE `kondisi`
  MODIFY `id_kondisi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `penerbit`
--
ALTER TABLE `penerbit`
  MODIFY `id_penerbit` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pengarang`
--
ALTER TABLE `pengarang`
  MODIFY `id_pengarang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `thn_terbit`
--
ALTER TABLE `thn_terbit`
  MODIFY `id_thn_terbit` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
