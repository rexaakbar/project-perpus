<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class PenerbitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 5 ; $i++) { 
	        DB::table('kategori')->insert([
	            'kategori' => str_random(15)
	        ]);
        }

        for ($i=0; $i < 5 ; $i++) { 
            DB::table('isbn')->insert([
                'isbn' => str_random(15)
            ]);
        }

        for ($i=0; $i < 5 ; $i++) { 
            DB::table('thn_terbit')->insert([
                'thn_terbit' => str_random(15)
            ]);
        }
    }
}
